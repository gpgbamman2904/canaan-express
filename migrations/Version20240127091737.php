<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240127091737 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE recrutements (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, nom_client VARCHAR(255) NOT NULL, prenom_client VARCHAR(255) NOT NULL, adresse_client LONGTEXT NOT NULL, phone_client VARCHAR(255) NOT NULL, option_f VARCHAR(255) DEFAULT NULL, nbr_mois_of INT DEFAULT NULL, reference VARCHAR(255) NOT NULL, formule VARCHAR(255) NOT NULL, nbr_mois INT NOT NULL, statut INT NOT NULL, fin_rea DATE NOT NULL, INDEX IDX_7F408CE8A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recrutements ADD CONSTRAINT FK_7F408CE8A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE recrutements DROP FOREIGN KEY FK_7F408CE8A76ED395');
        $this->addSql('DROP TABLE recrutements');
    }
}
