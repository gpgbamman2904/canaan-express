<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240214203415 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE decodeurs (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, stock_id INT DEFAULT NULL, numero VARCHAR(255) NOT NULL, statut TINYINT(1) NOT NULL, INDEX IDX_DA04C6DBA76ED395 (user_id), INDEX IDX_DA04C6DBDCD6110 (stock_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE decodeurs ADD CONSTRAINT FK_DA04C6DBA76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE decodeurs ADD CONSTRAINT FK_DA04C6DBDCD6110 FOREIGN KEY (stock_id) REFERENCES stocks (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE decodeurs DROP FOREIGN KEY FK_DA04C6DBA76ED395');
        $this->addSql('ALTER TABLE decodeurs DROP FOREIGN KEY FK_DA04C6DBDCD6110');
        $this->addSql('DROP TABLE decodeurs');
    }
}
