<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240131003256 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE complements ADD user_id INT DEFAULT NULL, ADD montant DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE complements ADD CONSTRAINT FK_3A429FA0A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('CREATE INDEX IDX_3A429FA0A76ED395 ON complements (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE complements DROP FOREIGN KEY FK_3A429FA0A76ED395');
        $this->addSql('DROP INDEX IDX_3A429FA0A76ED395 ON complements');
        $this->addSql('ALTER TABLE complements DROP user_id, DROP montant');
    }
}
