<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240207094309 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE stocks (id INT AUTO_INCREMENT NOT NULL, societe_id INT DEFAULT NULL, admi_id INT DEFAULT NULL, decodeur LONGTEXT NOT NULL, parabole INT NOT NULL, INDEX IDX_56F79805FCF77503 (societe_id), INDEX IDX_56F798059D44C234 (admi_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE stocks ADD CONSTRAINT FK_56F79805FCF77503 FOREIGN KEY (societe_id) REFERENCES societes (id)');
        $this->addSql('ALTER TABLE stocks ADD CONSTRAINT FK_56F798059D44C234 FOREIGN KEY (admi_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE stocks DROP FOREIGN KEY FK_56F79805FCF77503');
        $this->addSql('ALTER TABLE stocks DROP FOREIGN KEY FK_56F798059D44C234');
        $this->addSql('DROP TABLE stocks');
    }
}
