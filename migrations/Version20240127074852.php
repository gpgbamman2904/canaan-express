<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240127074852 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE societes (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, sec_act LONGTEXT NOT NULL, departement VARCHAR(255) NOT NULL, commune VARCHAR(255) NOT NULL, quartier VARCHAR(255) NOT NULL, latitude VARCHAR(255) DEFAULT NULL, longitude VARCHAR(255) DEFAULT NULL, adresse LONGTEXT DEFAULT NULL, nom_res VARCHAR(255) NOT NULL, prenom_res VARCHAR(255) NOT NULL, phone_res VARCHAR(255) NOT NULL, email_res VARCHAR(255) NOT NULL, observation LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users ADD societes_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E97E841BEA FOREIGN KEY (societes_id) REFERENCES societes (id)');
        $this->addSql('CREATE INDEX IDX_1483A5E97E841BEA ON users (societes_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E97E841BEA');
        $this->addSql('DROP TABLE societes');
        $this->addSql('DROP INDEX IDX_1483A5E97E841BEA ON users');
        $this->addSql('ALTER TABLE users DROP societes_id');
    }
}
