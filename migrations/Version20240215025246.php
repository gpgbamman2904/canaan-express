<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240215025246 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE rapports (id INT AUTO_INCREMENT NOT NULL, nbr_rea INT DEFAULT NULL, montant_rea DOUBLE PRECISION DEFAULT NULL, nbr_com INT DEFAULT NULL, montant_com DOUBLE PRECISION DEFAULT NULL, nbr_mig INT DEFAULT NULL, montant_mig DOUBLE PRECISION DEFAULT NULL, nbr_rec INT DEFAULT NULL, montant_rec DOUBLE PRECISION DEFAULT NULL, created_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, updated_at DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE rapports');
    }
}
