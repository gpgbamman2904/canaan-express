<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240206172141 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE recharges (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, admi_id INT DEFAULT NULL, montant DOUBLE PRECISION NOT NULL, INDEX IDX_7D4DE46A76ED395 (user_id), INDEX IDX_7D4DE469D44C234 (admi_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE recharges ADD CONSTRAINT FK_7D4DE46A76ED395 FOREIGN KEY (user_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE recharges ADD CONSTRAINT FK_7D4DE469D44C234 FOREIGN KEY (admi_id) REFERENCES users (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE recharges DROP FOREIGN KEY FK_7D4DE46A76ED395');
        $this->addSql('ALTER TABLE recharges DROP FOREIGN KEY FK_7D4DE469D44C234');
        $this->addSql('DROP TABLE recharges');
    }
}
