<?php

namespace App\Repository;

use App\Entity\Complements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Complements>
 *
 * @method Complements|null find($id, $lockMode = null, $lockVersion = null)
 * @method Complements|null findOneBy(array $criteria, array $orderBy = null)
 * @method Complements[]    findAll()
 * @method Complements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ComplementsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Complements::class);
    }

    public function add(Complements $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Complements $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   /**
    * @return Complements[] Returns an array of Complements objects
    */
    public function findByDate($value1, $value2, $value3): array
    {
        return $this->createQueryBuilder('c')
            ->Where('c.updatedAt > :val1')
            ->andWhere('c.updatedAt < :val2')
            ->andWhere('c.User = :val3')
            ->setParameter('val1', $value1)
            ->setParameter('val2', $value2)
            ->setParameter('val3', $value3)
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return Complements[] Returns an array of Complements objects
    */
    public function findByDateAdmin($value1, $value2): array
    {
        return $this->createQueryBuilder('c')
            ->Where('c.updatedAt > :val1')
            ->andWhere('c.updatedAt < :val2')
            ->setParameter('val1', $value1)
            ->setParameter('val2', $value2)
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?Complements
//    {
//        return $this->createQueryBuilder('c')
//            ->andWhere('c.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
