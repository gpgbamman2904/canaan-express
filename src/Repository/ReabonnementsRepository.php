<?php

namespace App\Repository;

use App\Entity\Reabonnements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Reabonnements>
 *
 * @method Reabonnements|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reabonnements|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reabonnements[]    findAll()
 * @method Reabonnements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReabonnementsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reabonnements::class);
    }

    public function add(Reabonnements $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Reabonnements $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   /**
    * @return Reabonnements[] Returns an array of Reabonnements objects
    */
   public function findByDate($value1, $value2, $value3): array
   {
       return $this->createQueryBuilder('r')
           ->Where('r.updatedAt BETWEEN :val1 AND :val2')
           ->andWhere('r.User = :val3')
           ->setParameter('val1', $value1)
           ->setParameter('val2', $value2)
           ->setParameter('val3', $value3)
           ->orderBy('r.id', 'DESC')
           ->getQuery()
           ->getResult()
       ;
   }

   /**
    * @return Reabonnements[] Returns an array of Reabonnements objects
    */
    public function findByDateAdmin($value1, $value2): array
    {
        return $this->createQueryBuilder('r')
            ->Where('r.updatedAt BETWEEN :val1 AND :val2')
            ->setParameter('val1', $value1)
            ->setParameter('val2', $value2)
            ->orderBy('r.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?Reabonnements
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
