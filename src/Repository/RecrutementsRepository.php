<?php

namespace App\Repository;

use App\Entity\Recrutements;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Recrutements>
 *
 * @method Recrutements|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recrutements|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recrutements[]    findAll()
 * @method Recrutements[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecrutementsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recrutements::class);
    }

    public function add(Recrutements $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Recrutements $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   /**
    * @return Recrutements[] Returns an array of Recrutements objects
    */
    public function findByDate($value1, $value2, $value3): array
    {
        return $this->createQueryBuilder('c')
            ->Where('c.updatedAt > :val1')
            ->andWhere('c.updatedAt < :val2')
            ->andWhere('c.user = :val3')
            ->setParameter('val1', $value1)
            ->setParameter('val2', $value2)
            ->setParameter('val3', $value3)
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return Recrutements[] Returns an array of Recrutements objects
    */
    public function findByDateAdmin($value1, $value2): array
    {
        return $this->createQueryBuilder('c')
            ->Where('c.updatedAt > :val1')
            ->andWhere('c.updatedAt < :val2')
            ->setParameter('val1', $value1)
            ->setParameter('val2', $value2)
            ->orderBy('c.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?Recrutements
//    {
//        return $this->createQueryBuilder('r')
//            ->andWhere('r.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
