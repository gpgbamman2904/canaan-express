<?php

namespace App\Repository;

use App\Entity\Migrations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Migrations>
 *
 * @method Migrations|null find($id, $lockMode = null, $lockVersion = null)
 * @method Migrations|null findOneBy(array $criteria, array $orderBy = null)
 * @method Migrations[]    findAll()
 * @method Migrations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MigrationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Migrations::class);
    }

    public function add(Migrations $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Migrations $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

   /**
    * @return Migrations[] Returns an array of Migrations objects
    */
    public function findByDate($value1, $value2, $value3): array
    {
        return $this->createQueryBuilder('m')
            ->Where('m.updatedAt > :val1')
            ->andWhere('m.updatedAt < :val2')
            ->andWhere('m.user = :val3')
            ->setParameter('val1', $value1)
            ->setParameter('val2', $value2)
            ->setParameter('val3', $value3)
            ->orderBy('m.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return Migrations[] Returns an array of Migrations objects
    */
    public function findByDateAdmin($value1, $value2): array
    {
        return $this->createQueryBuilder('m')
            ->Where('m.updatedAt > :val1')
            ->andWhere('m.updatedAt < :val2')
            ->setParameter('val1', $value1)
            ->setParameter('val2', $value2)
            ->orderBy('m.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

//    public function findOneBySomeField($value): ?Migrations
//    {
//        return $this->createQueryBuilder('m')
//            ->andWhere('m.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
