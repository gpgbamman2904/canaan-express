<?php

namespace App\Entity;

use App\Repository\RecrutementsRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RecrutementsRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Recrutements
{
    use Timestampable;
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $NomClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PrenomClient;

    /**
     * @ORM\Column(type="text")
     */
    private $AdresseClient;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $PhoneClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $OptionF;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbrMoisOF;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Reference;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Formule;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbrMois;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Statut;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="recrutements")
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $FinRea;

    /**
     * @ORM\Column(type="float")
     */
    private $Montant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomClient(): ?string
    {
        return $this->NomClient;
    }

    public function setNomClient(string $NomClient): self
    {
        $this->NomClient = $NomClient;

        return $this;
    }

    public function getPrenomClient(): ?string
    {
        return $this->PrenomClient;
    }

    public function setPrenomClient(string $PrenomClient): self
    {
        $this->PrenomClient = $PrenomClient;

        return $this;
    }

    public function getAdresseClient(): ?string
    {
        return $this->AdresseClient;
    }

    public function setAdresseClient(string $AdresseClient): self
    {
        $this->AdresseClient = $AdresseClient;

        return $this;
    }

    public function getPhoneClient(): ?string
    {
        return $this->PhoneClient;
    }

    public function setPhoneClient(string $PhoneClient): self
    {
        $this->PhoneClient = $PhoneClient;

        return $this;
    }

    public function getOptionF(): ?string
    {
        return $this->OptionF;
    }

    public function setOptionF(?string $OptionF): self
    {
        $this->OptionF = $OptionF;

        return $this;
    }

    public function getNbrMoisOF(): ?int
    {
        return $this->NbrMoisOF;
    }

    public function setNbrMoisOF(?int $NbrMoisOF): self
    {
        $this->NbrMoisOF = $NbrMoisOF;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->Reference;
    }

    public function setReference(string $Reference): self
    {
        $this->Reference = $Reference;

        return $this;
    }

    public function getFormule(): ?string
    {
        return $this->Formule;
    }

    public function setFormule(string $Formule): self
    {
        $this->Formule = $Formule;

        return $this;
    }

    public function getNbrMois(): ?int
    {
        return $this->NbrMois;
    }

    public function setNbrMois(int $NbrMois): self
    {
        $this->NbrMois = $NbrMois;

        return $this;
    }

    public function getStatut(): ?int
    {
        return $this->Statut;
    }

    public function setStatut(int $Statut): self
    {
        $this->Statut = $Statut;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFinRea(): ?\DateTimeInterface
    {
        return $this->FinRea;
    }

    public function setFinRea(\DateTimeInterface $FinRea): self
    {
        $this->FinRea = $FinRea;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->Montant;
    }

    public function setMontant(float $Montant): self
    {
        $this->Montant = $Montant;

        return $this;
    }
}
