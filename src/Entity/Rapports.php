<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\RapportsRepository;

/**
 * @ORM\Entity(repositoryClass=RapportsRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Rapports
{
    use Timestampable;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbrRea;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $MontantRea;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbrCom;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $MontantCom;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbrMig;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $MontantMig;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbrRec;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $MontantRec;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbrRea(): ?int
    {
        return $this->NbrRea;
    }

    public function setNbrRea(?int $NbrRea): self
    {
        $this->NbrRea = $NbrRea;

        return $this;
    }

    public function getMontantRea(): ?float
    {
        return $this->MontantRea;
    }

    public function setMontantRea(?float $MontantRea): self
    {
        $this->MontantRea = $MontantRea;

        return $this;
    }

    public function getNbrCom(): ?int
    {
        return $this->NbrCom;
    }

    public function setNbrCom(?int $NbrCom): self
    {
        $this->NbrCom = $NbrCom;

        return $this;
    }

    public function getMontantCom(): ?float
    {
        return $this->MontantCom;
    }

    public function setMontantCom(?float $MontantCom): self
    {
        $this->MontantCom = $MontantCom;

        return $this;
    }

    public function getNbrMig(): ?int
    {
        return $this->NbrMig;
    }

    public function setNbrMig(?int $NbrMig): self
    {
        $this->NbrMig = $NbrMig;

        return $this;
    }

    public function getMontantMig(): ?float
    {
        return $this->MontantMig;
    }

    public function setMontantMig(?float $MontantMig): self
    {
        $this->MontantMig = $MontantMig;

        return $this;
    }

    public function getNbrRec(): ?int
    {
        return $this->NbrRec;
    }

    public function setNbrRec(?int $NbrRec): self
    {
        $this->NbrRec = $NbrRec;

        return $this;
    }

    public function getMontantRec(): ?float
    {
        return $this->MontantRec;
    }

    public function setMontantRec(?float $MontantRec): self
    {
        $this->MontantRec = $MontantRec;

        return $this;
    }
}
