<?php

namespace App\Entity;

use App\Repository\MigrationsRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MigrationsRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Migrations
{
    use Timestampable;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NumAbonne;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $OldDecodeur;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="migrations")
     */
    private $user;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $FinRea;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NomClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PrenomClient;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $AdresseClient;

    /**
     * @ORM\Column(type="integer")
     */
    private $Statut;

    /**
     * @ORM\Column(type="float")
     */
    private $Montant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->Reference;
    }

    public function setReference(string $Reference): self
    {
        $this->Reference = $Reference;

        return $this;
    }

    public function getNumAbonne(): ?string
    {
        return $this->NumAbonne;
    }

    public function setNumAbonne(?string $NumAbonne): self
    {
        $this->NumAbonne = $NumAbonne;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->Phone;
    }

    public function setPhone(string $Phone): self
    {
        $this->Phone = $Phone;

        return $this;
    }

    public function getOldDecodeur(): ?string
    {
        return $this->OldDecodeur;
    }

    public function setOldDecodeur(string $OldDecodeur): self
    {
        $this->OldDecodeur = $OldDecodeur;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFinRea(): ?\DateTimeInterface
    {
        return $this->FinRea;
    }

    public function setFinRea(\DateTimeInterface $FinRea): self
    {
        $this->FinRea = $FinRea;

        return $this;
    }

    public function getNomClient(): ?string
    {
        return $this->NomClient;
    }

    public function setNomClient(string $NomClient): self
    {
        $this->NomClient = $NomClient;

        return $this;
    }

    public function getPrenomClient(): ?string
    {
        return $this->PrenomClient;
    }

    public function setPrenomClient(?string $PrenomClient): self
    {
        $this->PrenomClient = $PrenomClient;

        return $this;
    }

    public function getAdresseClient(): ?string
    {
        return $this->AdresseClient;
    }

    public function setAdresseClient(?string $AdresseClient): self
    {
        $this->AdresseClient = $AdresseClient;

        return $this;
    }

    public function getStatut(): ?int
    {
        return $this->Statut;
    }

    public function setStatut(int $Statut): self
    {
        $this->Statut = $Statut;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->Montant;
    }

    public function setMontant(float $Montant): self
    {
        $this->Montant = $Montant;

        return $this;
    }
}
