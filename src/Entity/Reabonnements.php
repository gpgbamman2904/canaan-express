<?php

namespace App\Entity;

use App\Repository\ReabonnementsRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReabonnementsRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Reabonnements
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NumAbonne;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Phone;

    /**
     * @ORM\Column(type="integer")
     */
    private $NbrMois;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Formule;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $upgrade;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $OptionF;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $NbrMoisOF;

    /**
     * @ORM\Column(type="integer")
     */
    private $Statut;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Observation;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="reabonnements")
     */
    private $User;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $FinRea;

    /**
     * @ORM\Column(type="float")
     */
    private $Montant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->Reference;
    }

    public function setReference(string $Reference): self
    {
        $this->Reference = $Reference;

        return $this;
    }

    public function getNumAbonne(): ?string
    {
        return $this->NumAbonne;
    }

    public function setNumAbonne(?string $NumAbonne): self
    {
        $this->NumAbonne = $NumAbonne;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->Phone;
    }

    public function setPhone(string $Phone): self
    {
        $this->Phone = $Phone;

        return $this;
    }

    public function getNbrMois(): ?int
    {
        return $this->NbrMois;
    }

    public function setNbrMois(int $NbrMois): self
    {
        $this->NbrMois = $NbrMois;

        return $this;
    }

    public function getFormule(): ?string
    {
        return $this->Formule;
    }

    public function setFormule(string $Formule): self
    {
        $this->Formule = $Formule;

        return $this;
    }

    public function getUpgrade(): ?string
    {
        return $this->upgrade;
    }

    public function setUpgrade(?string $upgrade): self
    {
        $this->upgrade = $upgrade;

        return $this;
    }

    public function getOptionF(): ?string
    {
        return $this->OptionF;
    }

    public function setOptionF(?string $OptionF): self
    {
        $this->OptionF = $OptionF;

        return $this;
    }

    public function getNbrMoisOF(): ?int
    {
        return $this->NbrMoisOF;
    }

    public function setNbrMoisOF(?int $NbrMoisOF): self
    {
        $this->NbrMoisOF = $NbrMoisOF;

        return $this;
    }

    public function getStatut(): ?int
    {
        return $this->Statut;
    }

    public function setStatut(int $Statut): self
    {
        $this->Statut = $Statut;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->Observation;
    }

    public function setObservation(?string $Observation): self
    {
        $this->Observation = $Observation;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->User;
    }

    public function setUser(?Users $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getFinRea(): ?\DateTimeInterface
    {
        return $this->FinRea;
    }

    public function setFinRea(?\DateTimeInterface $FinRea): self
    {
        $this->FinRea = $FinRea;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->Montant;
    }

    public function setMontant(float $Montant): self
    {
        $this->Montant = $Montant;

        return $this;
    }
}
