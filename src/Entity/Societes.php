<?php

namespace App\Entity;

use App\Repository\SocietesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SocietesRepository::class)
 */
class Societes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="text")
     */
    private $SecAct;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Departement;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Commune;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Quartier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Latitude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Longitude;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Adresse;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NomRes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PrenomRes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $PhoneRes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $EmailRes;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Observation;

    /**
     * @ORM\OneToMany(targetEntity=Users::class, mappedBy="societes")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity=Stocks::class, mappedBy="societe")
     */
    private $stocks;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->stocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getSecAct(): ?string
    {
        return $this->SecAct;
    }

    public function setSecAct(string $SecAct): self
    {
        $this->SecAct = $SecAct;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->Departement;
    }

    public function setDepartement(string $Departement): self
    {
        $this->Departement = $Departement;

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->Commune;
    }

    public function setCommune(string $Commune): self
    {
        $this->Commune = $Commune;

        return $this;
    }

    public function getQuartier(): ?string
    {
        return $this->Quartier;
    }

    public function setQuartier(string $Quartier): self
    {
        $this->Quartier = $Quartier;

        return $this;
    }

    public function getLatitude(): ?string
    {
        return $this->Latitude;
    }

    public function setLatitude(?string $Latitude): self
    {
        $this->Latitude = $Latitude;

        return $this;
    }

    public function getLongitude(): ?string
    {
        return $this->Longitude;
    }

    public function setLongitude(?string $Longitude): self
    {
        $this->Longitude = $Longitude;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->Adresse;
    }

    public function setAdresse(?string $Adresse): self
    {
        $this->Adresse = $Adresse;

        return $this;
    }

    public function getNomRes(): ?string
    {
        return $this->NomRes;
    }

    public function setNomRes(?string $NomRes): self
    {
        $this->NomRes = $NomRes;

        return $this;
    }

    public function getPrenomRes(): ?string
    {
        return $this->PrenomRes;
    }

    public function setPrenomRes(?string $PrenomRes): self
    {
        $this->PrenomRes = $PrenomRes;

        return $this;
    }

    public function getPhoneRes(): ?string
    {
        return $this->PhoneRes;
    }

    public function setPhoneRes(?string $PhoneRes): self
    {
        $this->PhoneRes = $PhoneRes;

        return $this;
    }

    public function getEmailRes(): ?string
    {
        return $this->EmailRes;
    }

    public function setEmailRes(?string $EmailRes): self
    {
        $this->EmailRes = $EmailRes;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->Observation;
    }

    public function setObservation(?string $Observation): self
    {
        $this->Observation = $Observation;

        return $this;
    }

    /**
     * @return Collection<int, Users>
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(Users $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setSocietes($this);
        }

        return $this;
    }

    public function removeUser(Users $user): self
    {
        if ($this->user->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getSocietes() === $this) {
                $user->setSocietes(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Stocks>
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stocks $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setSociete($this);
        }

        return $this;
    }

    public function removeStock(Stocks $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getSociete() === $this) {
                $stock->setSociete(null);
            }
        }

        return $this;
    }
}
