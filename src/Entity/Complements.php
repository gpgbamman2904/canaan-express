<?php

namespace App\Entity;

use App\Repository\ComplementsRepository;
use App\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ComplementsRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Complements
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Reference;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $NumAbonne;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $OldFormule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Formule;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Observation;

    /**
     * @ORM\Column(type="integer")
     */
    private $Statut;

    /**
     * @ORM\Column(type="float")
     */
    private $Montant;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="complements")
     */
    private $User;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getReference(): ?string
    {
        return $this->Reference;
    }

    public function setReference(string $Reference): self
    {
        $this->Reference = $Reference;

        return $this;
    }

    public function getNumAbonne(): ?string
    {
        return $this->NumAbonne;
    }

    public function setNumAbonne(?string $NumAbonne): self
    {
        $this->NumAbonne = $NumAbonne;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->Phone;
    }

    public function setPhone(string $Phone): self
    {
        $this->Phone = $Phone;

        return $this;
    }

    public function getOldFormule(): ?string
    {
        return $this->OldFormule;
    }

    public function setOldFormule(string $OldFormule): self
    {
        $this->OldFormule = $OldFormule;

        return $this;
    }

    public function getFormule(): ?string
    {
        return $this->Formule;
    }

    public function setFormule(string $Formule): self
    {
        $this->Formule = $Formule;

        return $this;
    }

    public function getObservation(): ?string
    {
        return $this->Observation;
    }

    public function setObservation(?string $Observation): self
    {
        $this->Observation = $Observation;

        return $this;
    }

    public function getStatut(): ?int
    {
        return $this->Statut;
    }

    public function setStatut(int $Statut): self
    {
        $this->Statut = $Statut;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->Montant;
    }

    public function setMontant(float $Montant): self
    {
        $this->Montant = $Montant;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->User;
    }

    public function setUser(?Users $User): self
    {
        $this->User = $User;

        return $this;
    }
}
