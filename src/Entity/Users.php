<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\UsersRepository;
use App\Entity\Traits\Timestampable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * @ORM\Entity(repositoryClass=UsersRepository::class)
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class Users implements UserInterface, PasswordAuthenticatedUserInterface
{
    use Timestampable;
    
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Prenoms;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Adresse;

    /**
     * @ORM\Column(type="boolean")
     */
    private $Sexe;

    /**
     * @ORM\ManyToOne(targetEntity=Societes::class, inversedBy="user")
     */
    private $societes;

    /**
     * @ORM\OneToMany(targetEntity=Migrations::class, mappedBy="user")
     */
    private $migrations;

    /**
     * @ORM\OneToMany(targetEntity=Reabonnements::class, mappedBy="User")
     */
    private $reabonnements;

    /**
     * @ORM\OneToMany(targetEntity=Recrutements::class, mappedBy="user")
     */
    private $recrutements;

    /**
     * @ORM\OneToMany(targetEntity=Complements::class, mappedBy="User")
     */
    private $complements;

    /**
     * @ORM\Column(type="float")
     */
    private $Solde;

    /**
     * @ORM\OneToMany(targetEntity=Commissions::class, mappedBy="user")
     */
    private $commissions;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $commission;

    /**
     * @ORM\OneToMany(targetEntity=Recharges::class, mappedBy="user")
     */
    private $recharges;

    /**
     * @ORM\OneToMany(targetEntity=Stocks::class, mappedBy="admi")
     */
    private $stocks;

    /**
     * @ORM\OneToMany(targetEntity=Decodeurs::class, mappedBy="user")
     */
    private $decodeurs;

    public function __construct()
    {
        $this->migrations = new ArrayCollection();
        $this->reabonnements = new ArrayCollection();
        $this->recrutements = new ArrayCollection();
        $this->complements = new ArrayCollection();
        $this->commissions = new ArrayCollection();
        $this->recharges = new ArrayCollection();
        $this->stocks = new ArrayCollection();
        $this->decodeurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @deprecated since Symfony 5.3, use getUserIdentifier instead
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getPrenoms(): ?string
    {
        return $this->Prenoms;
    }

    public function setPrenoms(string $Prenoms): self
    {
        $this->Prenoms = $Prenoms;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->Phone;
    }

    public function setPhone(string $Phone): self
    {
        $this->Phone = $Phone;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->Adresse;
    }

    public function setAdresse(string $Adresse): self
    {
        $this->Adresse = $Adresse;

        return $this;
    }

    public function isSexe(): ?bool
    {
        return $this->Sexe;
    }

    public function setSexe(bool $Sexe): self
    {
        $this->Sexe = $Sexe;

        return $this;
    }

    public function getSocietes(): ?Societes
    {
        return $this->societes;
    }

    public function setSocietes(?Societes $societes): self
    {
        $this->societes = $societes;

        return $this;
    }

    /**
     * @return Collection<int, Migrations>
     */
    public function getMigrations(): Collection
    {
        return $this->migrations;
    }

    public function addMigration(Migrations $migration): self
    {
        if (!$this->migrations->contains($migration)) {
            $this->migrations[] = $migration;
            $migration->setUser($this);
        }

        return $this;
    }

    public function removeMigration(Migrations $migration): self
    {
        if ($this->migrations->removeElement($migration)) {
            // set the owning side to null (unless already changed)
            if ($migration->getUser() === $this) {
                $migration->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Reabonnements>
     */
    public function getReabonnements(): Collection
    {
        return $this->reabonnements;
    }

    public function addReabonnement(Reabonnements $reabonnement): self
    {
        if (!$this->reabonnements->contains($reabonnement)) {
            $this->reabonnements[] = $reabonnement;
            $reabonnement->setUser($this);
        }

        return $this;
    }

    public function removeReabonnement(Reabonnements $reabonnement): self
    {
        if ($this->reabonnements->removeElement($reabonnement)) {
            // set the owning side to null (unless already changed)
            if ($reabonnement->getUser() === $this) {
                $reabonnement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Recrutements>
     */
    public function getRecrutements(): Collection
    {
        return $this->recrutements;
    }

    public function addRecrutement(Recrutements $recrutement): self
    {
        if (!$this->recrutements->contains($recrutement)) {
            $this->recrutements[] = $recrutement;
            $recrutement->setUser($this);
        }

        return $this;
    }

    public function removeRecrutement(Recrutements $recrutement): self
    {
        if ($this->recrutements->removeElement($recrutement)) {
            // set the owning side to null (unless already changed)
            if ($recrutement->getUser() === $this) {
                $recrutement->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Complements>
     */
    public function getComplements(): Collection
    {
        return $this->complements;
    }

    public function addComplement(Complements $complement): self
    {
        if (!$this->complements->contains($complement)) {
            $this->complements[] = $complement;
            $complement->setUser($this);
        }

        return $this;
    }

    public function removeComplement(Complements $complement): self
    {
        if ($this->complements->removeElement($complement)) {
            // set the owning side to null (unless already changed)
            if ($complement->getUser() === $this) {
                $complement->setUser(null);
            }
        }

        return $this;
    }

    public function getSolde(): ?float
    {
        return $this->Solde;
    }

    public function setSolde(float $Solde): self
    {
        $this->Solde = $Solde;

        return $this;
    }

    /**
     * @return Collection<int, Commissions>
     */
    public function getCommissions(): Collection
    {
        return $this->commissions;
    }

    public function addCommission(Commissions $commission): self
    {
        if (!$this->commissions->contains($commission)) {
            $this->commissions[] = $commission;
            $commission->setUser($this);
        }

        return $this;
    }

    public function removeCommission(Commissions $commission): self
    {
        if ($this->commissions->removeElement($commission)) {
            // set the owning side to null (unless already changed)
            if ($commission->getUser() === $this) {
                $commission->setUser(null);
            }
        }

        return $this;
    }

    public function getCommission(): ?float
    {
        return $this->commission;
    }

    public function setCommission(?float $commission): self
    {
        $this->commission = $commission;

        return $this;
    }

    /**
     * @return Collection<int, Recharges>
     */
    public function getRecharges(): Collection
    {
        return $this->recharges;
    }

    public function addRecharge(Recharges $recharge): self
    {
        if (!$this->recharges->contains($recharge)) {
            $this->recharges[] = $recharge;
            $recharge->setUser($this);
        }

        return $this;
    }

    public function removeRecharge(Recharges $recharge): self
    {
        if ($this->recharges->removeElement($recharge)) {
            // set the owning side to null (unless already changed)
            if ($recharge->getUser() === $this) {
                $recharge->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Stocks>
     */
    public function getStocks(): Collection
    {
        return $this->stocks;
    }

    public function addStock(Stocks $stock): self
    {
        if (!$this->stocks->contains($stock)) {
            $this->stocks[] = $stock;
            $stock->setAdmi($this);
        }

        return $this;
    }

    public function removeStock(Stocks $stock): self
    {
        if ($this->stocks->removeElement($stock)) {
            // set the owning side to null (unless already changed)
            if ($stock->getAdmi() === $this) {
                $stock->setAdmi(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Decodeurs>
     */
    public function getDecodeurs(): Collection
    {
        return $this->decodeurs;
    }

    public function addDecodeur(Decodeurs $decodeur): self
    {
        if (!$this->decodeurs->contains($decodeur)) {
            $this->decodeurs[] = $decodeur;
            $decodeur->setUser($this);
        }

        return $this;
    }

    public function removeDecodeur(Decodeurs $decodeur): self
    {
        if ($this->decodeurs->removeElement($decodeur)) {
            // set the owning side to null (unless already changed)
            if ($decodeur->getUser() === $this) {
                $decodeur->setUser(null);
            }
        }

        return $this;
    }
}
