<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Traits\Timestampable;
use App\Repository\StocksRepository;

/**
 * @ORM\Entity(repositoryClass=StocksRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class Stocks
{
    use Timestampable;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Societes::class, inversedBy="stocks")
     */
    private $societe;

    /**
     * @ORM\Column(type="integer")
     */
    private $parabole;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="stocks")
     */
    private $admi;

    /**
     * @ORM\OneToMany(targetEntity=Decodeurs::class, mappedBy="stock")
     */
    private $decodeurs;

    public function __construct()
    {
        $this->decodeurs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSociete(): ?Societes
    {
        return $this->societe;
    }

    public function setSociete(?Societes $societe): self
    {
        $this->societe = $societe;

        return $this;
    }

    public function getParabole(): ?int
    {
        return $this->parabole;
    }

    public function setParabole(int $parabole): self
    {
        $this->parabole = $parabole;

        return $this;
    }

    public function getAdmi(): ?Users
    {
        return $this->admi;
    }

    public function setAdmi(?Users $admi): self
    {
        $this->admi = $admi;

        return $this;
    }

    /**
     * @return Collection<int, Decodeurs>
     */
    public function getDecodeurs(): Collection
    {
        return $this->decodeurs;
    }

    public function addDecodeur(Decodeurs $decodeur): self
    {
        if (!$this->decodeurs->contains($decodeur)) {
            $this->decodeurs[] = $decodeur;
            $decodeur->setStock($this);
        }

        return $this;
    }

    public function removeDecodeur(Decodeurs $decodeur): self
    {
        if ($this->decodeurs->removeElement($decodeur)) {
            // set the owning side to null (unless already changed)
            if ($decodeur->getStock() === $this) {
                $decodeur->setStock(null);
            }
        }

        return $this;
    }
}
