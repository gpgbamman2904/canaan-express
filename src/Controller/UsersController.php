<?php

namespace App\Controller;

use App\Entity\Commissions;
use App\Entity\Societes;
use App\Entity\Users;
use App\Form\UsersType;
use App\Repository\UsersRepository;
use App\Repository\SocietesRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/dashboard/users")
 */
class UsersController extends AbstractController
{
    /**
     * @Route("/", name="app_users_index", methods={"GET"})
     * IsGranted("ROLE_CREATOR")
     */
    public function index(UsersRepository $usersRepository, SocietesRepository $societesRepository): Response
    {
        return $this->render('users/index.html.twig', [
            'users' => $usersRepository->findAll(),
            'societes' => $societesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/xred-redfrtcommf", name="app_revcomm", methods={"GET", "POST"})
     */
    public function solde(Request $request, EntityManagerInterface $entityManager, SocietesRepository $societesRepository, UsersRepository $usersRepository): Response
    {
        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('revcommtoken');
            if($this->isCsrfTokenValid('add-revcomm', $submittedToken)){
                $user = $this->getUser();
                $now = new DateTime();
                $now = $now->format('d');
                if($now >= "05" && $now <= "10"){
                    if($user->getCommission() >= $request->request->get('montant')){
                        $commi = new Commissions();
                        $commi->setMontant($request->request->get('montant'));
                        $commi->setUser($user);
                        $entityManager->persist($commi);
                        $entityManager->flush();

                        $user->setCommission($user->getCommission() - $request->request->get('montant'));
                        $user->setSolde($user->getSolde() + $request->request->get('montant'));
                        $entityManager->persist($user);
                        $entityManager->flush();
                        $this->addFlash(
                            'success',
                            'Commission Reversée !'
                        );
                    }else {
                        $this->addFlash(
                            'warning',
                            'Commission Insuffisant !'
                        );
                        return $this->redirectToRoute('app_users_show', ['id' => $user->getId()], Response::HTTP_SEE_OTHER);
                    }
                }else {
                    $this->addFlash(
                        'warning',
                        'Opération Impossible ! Veuillez contacter votre administrateur !'
                    );
                    return $this->redirectToRoute('app_users_show', ['id' => $user->getId()], Response::HTTP_SEE_OTHER);
                }
            }
        }

        return $this->redirectToRoute('app_users_show', ['id' => $user->getId()], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/new", name="app_users_new", methods={"GET", "POST"})
     * IsGranted("ROLE_CREATOR")
     */
    public function new(Request $request, UsersRepository $usersRepository, SocietesRepository $societesRepository, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = new Users();

        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Utoken');
            if($this->isCsrfTokenValid('add-user', $submittedToken)){
                if($request->request->get('societe') == "new"){

                    $societe = new Societes();
                    // Information de la nouvelle societé
                    $societe->setNom($request->request->get('so-name'));
                    $societe->setSecAct($request->request->get('secAct'));
                    $societe->setDepartement($request->request->get('departement'));
                    $societe->setCommune($request->request->get('commune'));
                    $societe->setQuartier($request->request->get('quartier'));
                    $societe->setAdresse($request->request->get('adsocie'));
                    $societe->setLatitude($request->request->get('latitude'));
                    $societe->setLongitude($request->request->get('longitude'));
                    $societe->setObservation($request->request->get('observation'));

                    $entityManager->persist($societe);
                    $entityManager->flush();
                }else {
                    $societe = $societesRepository->findOneBy(['id' => $request->request->get('societe')]);
                }

                $user->setPrenoms($request->request->get('prenom'));
                $user->setNom($request->request->get('nom'));
                $user->setEmail($request->request->get('email'));
                $user->setPhone($request->request->get('phone'));
                $user->setAdresse($request->request->get('adresse'));
                $user->setSocietes($societe);
                $gen = $request->request->get('genre');
                if ($gen = "Femme") {
                    $genre = false;
                } else {
                    $genre = true;
                }
                $user->setSexe($genre);
                $user->setUsername($request->request->get('identifiant'));
                // encode the plain password
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $request->request->get('password')
                    )
                );
                $user->setSolde(0);
    
                $entityManager->persist($user);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('app_users_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('users/new.html.twig', [
            'user' => $user,
            'societes' => $societesRepository->findAll(),
        ]);
    }


    /**
     * @Route("/{id}/change-password", name="app_users_cpwd", methods={"GET", "POST"})
     */
    public function cpwd(Users $user, Request $request, UsersRepository $usersRepository, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Cpwdtoken');
            if($this->isCsrfTokenValid('add-cpwd', $submittedToken)){

                // encode the plain password
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $request->request->get('password')
                    )
                );
    
                $entityManager->persist($user);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('app_orangep', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('security/cpwd.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}", name="app_users_show", methods={"GET"})
     */
    public function show(Users $user): Response
    {
        return $this->render('users/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_users_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Users $user, UsersRepository $usersRepository): Response
    {
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $usersRepository->add($user, true);

            return $this->redirectToRoute('app_users_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('users/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_users_delete", methods={"POST"})
     * IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Users $user, UsersRepository $usersRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $usersRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_users_index', [], Response::HTTP_SEE_OTHER);
    }
}
