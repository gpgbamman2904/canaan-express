<?php

namespace App\Controller;

use App\Entity\Rapports;
use App\Entity\Complements;
use App\Form\ComplementsType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ComplementsRepository;
use App\Repository\RapportsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/dashboard/complements")
 */
class ComplementsController extends AbstractController
{
    /**
     * @Route("/", name="app_complements_index", methods={"GET"})
     */
    public function index(ComplementsRepository $complementsRepository): Response
    {
        return $this->render('complements/index.html.twig', [
            'complements' => $complementsRepository->findBy(['User' => $this->getUser()], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/new", name="app_complements_new", methods={"GET", "POST"})
     */
    public function new(Request $request, RapportsRepository $rapportsRepository, ComplementsRepository $complementsRepository, EntityManagerInterface $entityManager): Response
    {
        $complement = new Complements();

        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Comtoken');
            if($this->isCsrfTokenValid('add-com', $submittedToken)){

                $user = $this->getUser();

                $complement->setReference($request->request->get('decodeur'));
                $complement->setNumAbonne($request->request->get('abonnee'));
                $complement->setPhone($request->request->get('phone'));
                $complement->setOldFormule($request->request->get('Aformule'));
                $complement->setFormule($request->request->get('Nformule'));
                $complement->setObservation($request->request->get('observation'));
                $complement->setUser($this->getUser());

                if($request->request->get('Aformule') == "Access"){
                    $montantAF = 5000;
                } elseif ($request->request->get('Aformule') == "Evasion") {
                    $montantAF = 10000;
                } elseif ($request->request->get('Aformule') == "Access +") {
                    $montantAF = 15000;
                } elseif ($request->request->get('Aformule') == "Evasion +") {
                    $montantAF = 20000;
                } elseif ($request->request->get('Aformule') == "Tout Canal +") {
                    $montantAF = 40000;
                }

                if($request->request->get('Nformule') == "Access"){
                    $montantNF = 5000;
                } elseif ($request->request->get('Nformule') == "Evasion") {
                    $montantNF = 10000;
                } elseif ($request->request->get('Nformule') == "Access +") {
                    $montantNF = 15000;
                } elseif ($request->request->get('Nformule') == "Evasion +") {
                    $montantNF = 20000;
                } elseif ($request->request->get('Nformule') == "Tout Canal +") {
                    $montantNF = 40000;
                }

                $montant = $montantNF - $montantAF;

                if ($user->getSolde() >= $montant) {
                    
                    $user->setSolde($user->getSolde() - $montant);
                    $user->setCommission($user->getCommission() + ($montant * 0.02));
                    $entityManager->persist($user);
                    $entityManager->flush();

                    $complement->setMontant($montant);
                    $complement->setStatut(false);
                    $entityManager->persist($complement);
                    $entityManager->flush();

                    $rapport = $rapportsRepository->findBy([], ['id' => 'DESC'], $limit=1);
                    if ($rapport) {
                        $dateR = $rapport[0]->getCreatedAt();
                        $now = date("Y/m/d");
                        if ($now == $dateR->format("Y/m/d") ) {
                            $NbrCom = $rapport[0]->getNbrCom() + 1 ;
                            $MontantCom = $rapport[0]->getMontantCom() + $montant ;
                            $rapport[0]->setNbrCom($NbrCom);
                            $rapport[0]->setMontantCom($MontantCom);
                            $entityManager->persist($rapport[0]);
                            $entityManager->flush();
                        } else {
                            $rapport = new Rapports();
                            $rapport->setNbrCom(1);
                            $rapport->setMontantCom($montant);
                            $entityManager->persist($rapport);
                            $entityManager->flush();
                        }
                    } else {
                        $rapport = new Rapports();
                        $rapport->setNbrCom(1);
                        $rapport->setMontantCom($montant);
                        $entityManager->persist($rapport);
                        $entityManager->flush();
                    }

                    $this->addFlash(
                        'success',
                        'Compléments lancé. En attente de validation !'
                    );

                    return $this->redirectToRoute('app_complements_index', [], Response::HTTP_SEE_OTHER);
                }else {
                    $this->addFlash(
                       'warning',
                       'Fonds Insuffisant ! Veuillez contact votre administrateur '
                    );
                    return $this->redirectToRoute('app_complements_new', [], Response::HTTP_SEE_OTHER);
                }
            }
        }
        return $this->renderForm('complements/new.html.twig', [
            'complement' => $complement,
        ]);
    }

    /**
     * @Route("/{id}", name="app_complements_show", methods={"GET"})
     */
    public function show(Complements $complement): Response
    {
        return $this->render('complements/show.html.twig', [
            'complement' => $complement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_complements_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Complements $complement, ComplementsRepository $complementsRepository): Response
    {
        $form = $this->createForm(ComplementsType::class, $complement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $complementsRepository->add($complement, true);

            return $this->redirectToRoute('app_complements_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('complements/edit.html.twig', [
            'complement' => $complement,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_complements_delete", methods={"POST"})
     */
    public function delete(Request $request, Complements $complement, ComplementsRepository $complementsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$complement->getId(), $request->request->get('_token'))) {
            $complementsRepository->remove($complement, true);
        }

        return $this->redirectToRoute('app_complements_index', [], Response::HTTP_SEE_OTHER);
    }
}
