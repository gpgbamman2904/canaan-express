<?php

namespace App\Controller;

use App\Entity\Rapports;
use App\Entity\Reabonnements;
use App\Form\ReabonnementsType;
use App\Repository\RapportsRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ReabonnementsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/dashboard/reabonnements")
 */
class ReabonnementsController extends AbstractController
{
    private $security;

    public function __construct(Security $security, UrlGeneratorInterface $urlGenerator)
    {
        $this->security = $security;
        $this->urlGenerator = $urlGenerator;
    }
    
    /**
     * @Route("/", name="app_reabonnements_index", methods={"GET"})
     */
    public function index(ReabonnementsRepository $reabonnementsRepository): Response
    {

        return $this->render('reabonnements/index.html.twig', [
            'reabonnements' => $reabonnementsRepository->findBy(['User' => $this->getUser()], ['id' => 'DESC']),
        ]);
    }
    
        
    /**
     * @Route("/reabonnement-liste", name="app_rea_liste")
     */
    public function realiste(ReabonnementsRepository $reabonnementsRepository): Response
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('app_reabonnements_encours');
        }
        
        return $this->render('reabonnements/realiste.html.twig', [
            'reabonnements' => $reabonnementsRepository->findBy([], ['id' => 'DESC']),
        ]);

    }

    /**
     * @Route("/new", name="app_reabonnements_new", methods={"GET", "POST"})
     */
    public function new(Request $request, RapportsRepository $rapportsRepository, ReabonnementsRepository $reabonnementsRepository, EntityManagerInterface $entityManager): Response
    {
        $reabonnement = new Reabonnements();
        $user = $this->getUser();

        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Reatoken');
            if($this->isCsrfTokenValid('add-rea', $submittedToken)){

                if($request->request->get('formule') == "Access"){
                    $montantF = 5000 * $request->request->get('mois');
                } elseif ($request->request->get('formule') == "Evasion") {
                    $montantF = 10000 * $request->request->get('mois');
                } elseif ($request->request->get('formule') == "Access +") {
                    $montantF = 15000 * $request->request->get('mois');
                } elseif ($request->request->get('formule') == "Evasion +") {
                    $montantF = 20000 * $request->request->get('mois');
                } elseif ($request->request->get('formule') == "Tout Canal +") {
                    $montantF = 40000 * $request->request->get('mois');
                }

                if ($request->request->get('nopt')) {
                    $nopt = $request->request->get('nopt');
                } else {
                    $nopt = 0;
                }

                if($request->request->get('option') == "Option CHARME"){
                    $montantOF = 6000 * $nopt;
                } elseif ($request->request->get('option') == "DSTV ENGLISH BASIC") {
                    $montantOF = 5000  * $nopt;
                } elseif ($request->request->get('option') == "DSTV ENGLISH PLUS") {
                    $montantOF = 13000  * $nopt;
                } else{
                    $montantOF = 0;
                }

                $montant = $montantF + $montantOF;

                if ($user->getSolde() >= $montant) {
                    
                    $user->setSolde($user->getSolde() - $montant);
                    $user->setCommission($user->getCommission() + ($montantF * 0.02));
                    $entityManager->persist($user);
                    $entityManager->flush();

                    $reabonnement->setReference($request->request->get('decodeur'));
                    $reabonnement->setPhone($request->request->get('phone'));
                    $reabonnement->setFormule($request->request->get('formule'));
                    $reabonnement->setNbrMois($request->request->get('mois'));
                    $reabonnement->setOptionF($request->request->get('option'));
                    $reabonnement->setNbrMoisOF($nopt);
                    $reabonnement->setObservation($request->request->get('observation'));
                    $reabonnement->setUser($user);
                    $reabonnement->setMontant($montantF + $montantOF);
                    $reabonnement->setStatut(false);
                    $entityManager->persist($reabonnement);
                    $entityManager->flush();

                    $rapport = $rapportsRepository->findBy([], ['id' => 'DESC'], $limit=1);
                    if ($rapport) {
                        $dateR = $rapport[0]->getCreatedAt();
                        $now = date("Y/m/d");
                        if ($now == $dateR->format("Y/m/d") ) {
                            $NbrRea = $rapport[0]->getNbrRea() + 1 ;
                            $MontantRea = $rapport[0]->getMontantRea() + $montant ;
                            $rapport[0]->setNbrRea($NbrRea);
                            $rapport[0]->setMontantRea($MontantRea);
                            $entityManager->persist($rapport[0]);
                            $entityManager->flush();
                        } else {
                            $rapport = new Rapports();
                            $rapport->setNbrRea(1);
                            $rapport->setMontantRea($montant);
                            $entityManager->persist($rapport);
                            $entityManager->flush();
                        }
                    } else {
                        $rapport = new Rapports();
                        $rapport->setNbrRea(1);
                        $rapport->setMontantRea($montant);
                        $entityManager->persist($rapport);
                        $entityManager->flush();
                    }

                    $this->addFlash(
                        'success',
                        'Réabonnement lancé. En attente de validation !'
                     );
                    return $this->redirectToRoute('app_reabonnements_index', [], Response::HTTP_SEE_OTHER);
                }else {
                    $this->addFlash(
                       'warning',
                       'Fonds Insuffisant ! Veuillez contact votre administrateur '
                    );
                    return $this->redirectToRoute('app_reabonnements_new', [], Response::HTTP_SEE_OTHER);
                }
            }
        }

        return $this->renderForm('reabonnements/new.html.twig', [
            'reabonnement' => $reabonnement,
        ]);
    }

    /**
     * @Route("/{id}", name="app_reabonnements_show", methods={"GET"})
     */
    public function show(Reabonnements $reabonnement): Response
    {
        return $this->render('reabonnements/show.html.twig', [
            'reabonnement' => $reabonnement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_reabonnements_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Reabonnements $reabonnement, ReabonnementsRepository $reabonnementsRepository): Response
    {
        $form = $this->createForm(ReabonnementsType::class, $reabonnement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $reabonnementsRepository->add($reabonnement, true);

            return $this->redirectToRoute('app_reabonnements_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('reabonnements/edit.html.twig', [
            'reabonnement' => $reabonnement,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_reabonnements_delete", methods={"POST"})
     */
    public function delete(Request $request, Reabonnements $reabonnement, ReabonnementsRepository $reabonnementsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$reabonnement->getId(), $request->request->get('_token'))) {
            $reabonnementsRepository->remove($reabonnement, true);
        }

        return $this->redirectToRoute('app_reabonnements_index', [], Response::HTTP_SEE_OTHER);
    }
}
