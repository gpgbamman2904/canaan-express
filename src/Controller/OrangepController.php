<?php

namespace App\Controller;

use DateTime;
use DateInterval;
use App\Repository\MigrationsRepository;
use App\Repository\ComplementsRepository;
use App\Repository\DecodeursRepository;
use App\Repository\RecrutementsRepository;
use App\Repository\ReabonnementsRepository;
use App\Repository\RechargesRepository;
use App\Repository\SocietesRepository;
use App\Repository\StocksRepository;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class OrangepController extends AbstractController
{
    private $security;

    public function __construct(Security $security, UrlGeneratorInterface $urlGenerator)
    {
        $this->security = $security;
        $this->urlGenerator = $urlGenerator;
    }
    
    /**
     * @Route("/dashboard", name="app_dash")
     */
    public function dashboard(DecodeursRepository $decodeursRepository, ReabonnementsRepository $reabonnementsRepository, RecrutementsRepository $recrutementsRepository, MigrationsRepository $migrationsRepository, ComplementsRepository $complementsRepository, RechargesRepository $rechargesRepository, SocietesRepository $societesRepository, StocksRepository $stocksRepository): Response
    {
        $now = date("Y/m/d H:i:s");
        $d7 = strtotime("-7 Days");
        $dat7 = date("Y/m/d H:i:s", $d7);
        $d30 = strtotime("-30 Days");
        $dat30 = date("Y/m/d H:i:s", $d30);
        $recharges = $rechargesRepository->findBy(['user' => $this->getUser()], ['id' => 'DESC'], $limit=5);
        $decodeurs = $decodeursRepository->findBy(['user' => $this->getUser(), 'statut'=>false]);

        // dd($reabonnementsRepository->findByDate($dat7, $now, $this->getUser()));

        return $this->render('orangep/index.html.twig', [
            'rea7' => $reabonnementsRepository->findByDate($dat7, $now, $this->getUser()),
            'rea30' => $reabonnementsRepository->findByDate($dat30, $now, $this->getUser()),
            'rec7' =>$recrutementsRepository->findByDate($dat7, $now, $this->getUser()),
            'rec30' =>$recrutementsRepository->findByDate($dat30, $now, $this->getUser()),
            'mig7' =>$migrationsRepository->findByDate($dat7, $now, $this->getUser()),
            'mig30' =>$migrationsRepository->findByDate($dat30, $now, $this->getUser()),
            'com7' =>$complementsRepository->findByDate($dat7, $now, $this->getUser()),
            'com30' =>$complementsRepository->findByDate($dat30, $now, $this->getUser()),
            'reabonnements' => $reabonnementsRepository->findBy(['User' => $this->getUser()]),
            'reas' =>$reabonnementsRepository->findBy(['User' => $this->getUser()], ['id' => 'DESC'], $limit=5),
            'recrutements' => $recrutementsRepository->findBy(['user' => $this->getUser()]),
            'complements' => $complementsRepository->findBy(['User' => $this->getUser()]),
            'migrations' => $migrationsRepository->findBy(['user' => $this->getUser()]),
            'recharges'  => $recharges,
            'decodeurs'  => $decodeurs,
        ]);
    }
    
    /**
     * @Route("/dashboard/user/statistique", name="app_user_statistique")
     */
    public function statistique(ReabonnementsRepository $reabonnementsRepository, RecrutementsRepository $recrutementsRepository, MigrationsRepository $migrationsRepository, ComplementsRepository $complementsRepository, RechargesRepository $rechargesRepository, SocietesRepository $societesRepository, StocksRepository $stocksRepository): Response
    {
        $now = date("Y/m/d H:i:s");
        $d = date("Y/m/d 00:00:00");
        $d7 = strtotime("-7 Days");
        $dat7 = date("Y/m/d H:i:s", $d7);
        $d30 = strtotime("-30 Days");
        $dat30 = date("Y/m/d H:i:s", $d30);

        // dd($reabonnementsRepository->findByDate($dat7, $now, $this->getUser()));

        return $this->render('orangep/statistique.html.twig', [
            'rea' => $reabonnementsRepository->findByDate($d, $now, $this->getUser()),
            'rea7' => $reabonnementsRepository->findByDate($dat7, $now, $this->getUser()),
            'rea30' => $reabonnementsRepository->findByDate($dat30, $now, $this->getUser()),
            'rec' =>$recrutementsRepository->findByDate($d, $now, $this->getUser()),
            'rec7' =>$recrutementsRepository->findByDate($dat7, $now, $this->getUser()),
            'rec30' =>$recrutementsRepository->findByDate($dat30, $now, $this->getUser()),
            'mig' =>$migrationsRepository->findByDate($d, $now, $this->getUser()),
            'mig7' =>$migrationsRepository->findByDate($dat7, $now, $this->getUser()),
            'mig30' =>$migrationsRepository->findByDate($dat30, $now, $this->getUser()),
            'com' =>$complementsRepository->findByDate($d, $now, $this->getUser()),
            'com7' =>$complementsRepository->findByDate($dat7, $now, $this->getUser()),
            'com30' =>$complementsRepository->findByDate($dat30, $now, $this->getUser()),
        ]);
    }

    /**
     * @Route("/orangep", name="app_orangep")
     */
    public function index(): Response
    {
        if ($this->security->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('app_admin');
        }elseif ($this->security->isGranted('ROLE_CREATOR')) {
            return $this->redirectToRoute('app_users_index');
        }

        return $this->redirectToRoute('app_dash');
    }
}
