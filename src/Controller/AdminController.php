<?php

namespace App\Controller;

use App\Entity\Complements;
use App\Entity\Decodeurs;
use App\Entity\Migrations;
use App\Entity\Recrutements;
use App\Entity\Reabonnements;
use App\Entity\Recharges;
use App\Entity\Stocks;
use App\Entity\Users;
use App\Repository\ComplementsRepository;
use App\Repository\MigrationsRepository;
use App\Repository\RapportsRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RecrutementsRepository;
use App\Repository\ReabonnementsRepository;
use App\Repository\RechargesRepository;
use App\Repository\SocietesRepository;
use App\Repository\StocksRepository;
use App\Repository\UsersRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="app_admin")
     * IsGranted("ROLE_ADMIN")
     */
    public function index(Request $request, SocietesRepository $societesRepository, ReabonnementsRepository $reabonnementsRepository, RecrutementsRepository $recrutementsRepository, MigrationsRepository $migrationsRepository, ComplementsRepository $complementsRepository, RechargesRepository $rechargesRepository): Response
    {
        $now = date("Y/m/d H:i:s");
        $d7 = strtotime("-7 Days");
        $dat7 = date("Y/m/d H:i:s", $d7);
        $d30 = strtotime("-30 Days");
        $dat30 = date("Y/m/d H:i:s", $d30);
        $recharges = $rechargesRepository->findBy([], ['id' => 'DESC'], $limit=5);

        // dd($reabonnementsRepository->findByDate($dat7, $now, $this->getUser()));

        return $this->render('admin/index.html.twig', [
            'rea7' => $reabonnementsRepository->findByDateAdmin($dat7, $now),
            'rea30' => $reabonnementsRepository->findByDateAdmin($dat30, $now),
            'rec7' =>$recrutementsRepository->findByDateAdmin($dat7, $now),
            'rec30' =>$recrutementsRepository->findByDateAdmin($dat30, $now),
            'mig7' =>$migrationsRepository->findByDateAdmin($dat7, $now),
            'mig30' =>$migrationsRepository->findByDateAdmin($dat30, $now),
            'com7' =>$complementsRepository->findByDateAdmin($dat7, $now),
            'com30' =>$complementsRepository->findByDateAdmin($dat30, $now),
            'reabonnements' => $reabonnementsRepository->findAll(),
            'reas' =>$reabonnementsRepository->findBy([], ['id' => 'DESC'], $limit=5),
            'recrutements' => $recrutementsRepository->findAll(),
            'complements' => $complementsRepository->findAll(),
            'migrations' => $migrationsRepository->findAll(),
            'recharges'  => $recharges,
            'societes' => $societesRepository->findAll(),
        ]);
    }
    
    /**
     * @Route("/{id}/change-password-admin", name="app_users_acpwd", methods={"GET", "POST"})
     */
    public function cpwd(Users $user, Request $request, UsersRepository $usersRepository, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Cpwdtoken');
            if($this->isCsrfTokenValid('add-cpwd', $submittedToken)){

                // encode the plain password
                $user->setPassword(
                    $userPasswordHasher->hashPassword(
                        $user,
                        $request->request->get('password')
                    )
                );
    
                $entityManager->persist($user);
                $entityManager->flush();
                // do anything else you need here, like send an email

                return $this->redirectToRoute('app_orangep', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->renderForm('security/acpwd.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/user/{id}", name="app_ausers_show", methods={"GET"})
     */
    public function show(Users $user): Response
    {
        return $this->render('admin/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/xred-ajtsld", name="app_ajsolde", methods={"GET", "POST"})
     * IsGranted("ROLE_ADMIN")
     */
    public function solde(Request $request, EntityManagerInterface $entityManager, SocietesRepository $societesRepository, UsersRepository $usersRepository): Response
    {
        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Stoken');
            if($this->isCsrfTokenValid('add-solde', $submittedToken)){
                $recharge = new Recharges();
                $societe = $societesRepository->findOneBy(['id' => $request->request->get('id')]);
                $user = $usersRepository->findOneBy(['societes' => $societe]);

                $recharge->setMontant($request->request->get('solde'));
                $recharge->setUser($user);
                $recharge->setAdmi($this->getUser());
                $entityManager->persist($recharge);
                $entityManager->flush();

                $user->setSolde($user->getSolde() + $request->request->get('solde'));
                $entityManager->persist($user);
                $entityManager->flush();
                
                $this->addFlash(
                    'success',
                    'Solde Rechargé !'
                );
                return $this->redirectToRoute('app_users_index', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->render('admin/ajouts.html.twig', [
            'societes' => $societesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/reabonnement/en-cours", name="app_reabonnements_encours", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function reaencours(ReabonnementsRepository $reabonnementsRepository): Response
    {
        return $this->render('admin/reaencours.html.twig', [
            'reabonnements' => $reabonnementsRepository->findBy(['Statut' => false], ['id' => 'ASC']),
        ]);
    }

    /**
     * @Route("/{id}/reabonnement/activation", name="app_reabonnements_activation", methods={"GET", "POST"})
     * IsGranted("ROLE_ADMIN")
     */
    public function reaactivation(Request $request, ReabonnementsRepository $reabonnementsRepository, EntityManagerInterface $entityManager, Reabonnements $reabonnement): Response
    {
        if ($this->isCsrfTokenValid('reaactivation'.$reabonnement->getId(), $request->request->get('reatoken'))) {
            $reabonnement->setStatut(true);
            $dat = $reabonnement->getUpdatedAt();
            $ad = 30 * $reabonnement->getNbrMois();
            $finr = $dat->modify($ad.' day');
            $reabonnement->setFinRea($finr);
            $entityManager->persist($reabonnement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_reabonnements_encours', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/reabonnement/active", name="app_reabonnements_active", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function reaactive(ReabonnementsRepository $reabonnementsRepository): Response
    {
        return $this->render('admin/reaactive.html.twig', [
            'reabonnements' => $reabonnementsRepository->findBy(['Statut' => true], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/recrutement/en-cours", name="app_recrutements_encours", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function recencours(RecrutementsRepository $recrutementsRepository): Response
    {
        return $this->render('admin/recencours.html.twig', [
            'recrutements' => $recrutementsRepository->findBy(['Statut' => false], ['id' => 'ASC']),
        ]);
    }

    /**
     * @Route("/{id}/recrutement/activation", name="app_recrutements_activation", methods={"GET", "POST"})
     * IsGranted("ROLE_ADMIN")
     */
    public function recactivation(Request $request, RecrutementsRepository $recrutementsRepository, EntityManagerInterface $entityManager, Recrutements $recrutement): Response
    {
        if ($this->isCsrfTokenValid('recactivation'.$recrutement->getId(), $request->request->get('rectoken'))) {
            $recrutement->setStatut(true);
            $dat = $recrutement->getUpdatedAt();
            $ad = 30 ;
            $finr = $dat->modify('30 day');
            $recrutement->setFinRea($finr);
            $entityManager->persist($recrutement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_recrutements_encours', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/recrutements/active", name="app_recrutements_active", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function recactive(RecrutementsRepository $recrutementsRepository): Response
    {
        return $this->render('admin/recactive.html.twig', [
            'recrutements' => $recrutementsRepository->findBy(['Statut' => true], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/migration/en-cours", name="app_migrations_encours", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function migencours(MigrationsRepository $migrationsRepository): Response
    {
        return $this->render('admin/migencours.html.twig', [
            'migrations' => $migrationsRepository->findBy(['Statut' => false], ['id' => 'ASC']),
        ]);
    }

    /**
     * @Route("/{id}/migration/activation", name="app_migrations_activation", methods={"GET", "POST"})
     * IsGranted("ROLE_ADMIN")
     */
    public function migactivation(Request $request, MigrationsRepository $MigrationsRepository, EntityManagerInterface $entityManager, Migrations $migration): Response
    {
        if ($this->isCsrfTokenValid('migactivation'.$migration->getId(), $request->request->get('migtoken'))) {
            $migration->setStatut(true);
            $dat = $migration->getUpdatedAt();
            $migration->setFinRea($dat->modify('+30 day'));
            $entityManager->persist($migration);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_migrations_encours', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/migrations/active", name="app_migrations_active", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function migactive(MigrationsRepository $migrationsRepository): Response
    {
        return $this->render('admin/migactive.html.twig', [
            'migrations' => $migrationsRepository->findBy(['Statut' => true], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/complements/en-cours", name="app_complements_encours", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function comencours(ComplementsRepository $complementsRepository): Response
    {
        return $this->render('admin/comencours.html.twig', [
            'complements' => $complementsRepository->findBy(['Statut' => false], ['id' => 'ASC']),
        ]);
    }

    /**
     * @Route("/{id}/complement/activation", name="app_complements_activation", methods={"GET", "POST"})
     * IsGranted("ROLE_ADMIN")
     */
    public function comactivation(Request $request, EntityManagerInterface $entityManager, Complements $complement): Response
    {
        if ($this->isCsrfTokenValid('comactivation'.$complement->getId(), $request->request->get('comtoken'))) {
            $complement->setStatut(true);
            $entityManager->persist($complement);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_complements_encours', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/complements/active", name="app_complements_active", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function comactive(ComplementsRepository $complementsRepository): Response
    {
        return $this->render('admin/comactive.html.twig', [
            'complements' => $complementsRepository->findBy(['Statut' => true], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/statistique", name="app_statistique", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function statistique(UsersRepository $usersRepository): Response
    {
        // $users = $usersRepository->findAll();
        return $this->render('admin/statistique.html.twig', [
            'users' => $usersRepository->findAll(),
        ]);
    }
    
    /**
     * @Route("/recharges", name="app_recharges", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function recharges(UsersRepository $usersRepository, RechargesRepository $rechargesRepository): Response
    {
        
        return $this->render('admin/recharges.html.twig', [
            'recharges'  => $rechargesRepository->findBy([], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/rapports", name="app_rapports", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function rapports(UsersRepository $usersRepository, RapportsRepository $rapportsRepository, ReabonnementsRepository $reabonnementsRepository, RecrutementsRepository $recrutementsRepository, MigrationsRepository $migrationsRepository, ComplementsRepository $complementsRepository): Response
    {
        $d = date("Y/m/d 00:00:00");
        $debut = date("2024/02/01 00:00:00");
        $now = date("Y/m/d H:i:s");
        $d30 = strtotime("-30 Days");
        $dat30 = date("Y/m/d H:i:s", $d30);


        // while ($d > $debut) {
        // }

        return $this->render('admin/rapports.html.twig', [
            'rea7' => $reabonnementsRepository->findByDateAdmin($d, $now),
            'rea30' => $reabonnementsRepository->findByDateAdmin($dat30, $now),
            'rec7' =>$recrutementsRepository->findByDateAdmin($d, $now),
            'rec30' =>$recrutementsRepository->findByDateAdmin($dat30, $now),
            'mig7' =>$migrationsRepository->findByDateAdmin($d, $now),
            'mig30' =>$migrationsRepository->findByDateAdmin($dat30, $now),
            'com7' =>$complementsRepository->findByDateAdmin($d, $now),
            'com30' =>$complementsRepository->findByDateAdmin($dat30, $now),
            'reabonnements' => $reabonnementsRepository->findAll(),
            'recrutements' => $recrutementsRepository->findAll(),
            'complements' => $reabonnementsRepository->findAll(),
            'migrations' => $recrutementsRepository->findAll(),
            'rapports' => $rapportsRepository->findAll(),
        ]);
    }

    /**
     * @Route("/stock-liste", name="app_stock_liste", methods={"GET"})
     * IsGranted("ROLE_ADMIN")
     */
    public function stocks(Request $request, StocksRepository $stocksRepository): Response
    {
        // $users = $usersRepository->findAll();
        return $this->render('admin/stocks.html.twig', [
            'stocks' => $stocksRepository->findAll(),
        ]);
    }

    /**
     * @Route("/stock-ajout", name="app_stock_ajout", methods={"GET", "POST"})
     * IsGranted("ROLE_ADMIN")
     */
    public function ajoutstocks(Request $request, StocksRepository $stocksRepository, SocietesRepository $societesRepository, EntityManagerInterface $entityManager, UsersRepository $usersRepository): Response
    {
        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Stocktoken');
            if($this->isCsrfTokenValid('add-stock', $submittedToken)){
                $stock = new Stocks();
                $decodeurs = $request->request->get('decodeur');
                $societe = $societesRepository->findOneBy(['id' => $request->request->get('id')]);
                $user = $usersRepository->findOneBy(['societes' => $societe]);

                $stock->setSociete($societe);
                $stock->setParabole($request->request->get('parabole'));
                $stock->setAdmi($this->getUser());

                $entityManager->persist($stock);
                $entityManager->flush();
                
                foreach( $decodeurs as $decodeur){
                    $dec = new Decodeurs();

                    $dec->setUser($user);
                    $dec->setNumero($decodeur);
                    $dec->setStock($stock);
                    $dec->setStatut(false);
                    $entityManager->persist($dec);
                    $entityManager->flush();

                }

                $this->addFlash(
                    'success',
                    'Stock Ajouté avec success !'
                );
                return $this->redirectToRoute('app_stock_liste', [], Response::HTTP_SEE_OTHER);
            }
        }

        return $this->render('admin/ajoutstock.html.twig', [
            'stocks' => $stocksRepository->findAll(),
            'societes' => $societesRepository->findAll(),
        ]);
    }
}
