<?php

namespace App\Controller;

use App\Entity\Rapports;
use App\Entity\Migrations;
use App\Form\MigrationsType;
use App\Repository\MigrationsRepository;
use App\Repository\RapportsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/dashboard/migrations")
 */
class MigrationsController extends AbstractController
{
    /**
     * @Route("/", name="app_migrations_index", methods={"GET"})
     */
    public function index(MigrationsRepository $migrationsRepository): Response
    {
        return $this->render('migrations/index.html.twig', [
            'migrations' => $migrationsRepository->findBy(['user' => $this->getUser()], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/new", name="app_migrations_new", methods={"GET", "POST"})
     */
    public function new(Request $request, RapportsRepository $rapportsRepository, MigrationsRepository $migrationsRepository, EntityManagerInterface $entityManager): Response
    {
        $migration = new Migrations();

        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Migtoken');
            if($this->isCsrfTokenValid('add-mig', $submittedToken)){
                $montant = 1000;
                $user = $this->getUser();
                if ($user->getSolde() >= $montant) {

                    $user->setSolde($user->getSolde() - $montant);
                    $entityManager->persist($user);
                    $entityManager->flush();

                    $migration->setNomClient($request->request->get('nom'));
                    $migration->setPrenomClient($request->request->get('prenom'));
                    $migration->setPhone($request->request->get('phone'));
                    $migration->setAdresseClient($request->request->get('quartier'));
                    $migration->setReference($request->request->get('Ndecodeur'));
                    $migration->setOldDecodeur($request->request->get('Adecodeur'));
                    $migration->setUser($this->getUser());
                    $migration->setMontant(1000);
                    $migration->setStatut(false);
                    $entityManager->persist($migration);
                    $entityManager->flush();

                    $rapport = $rapportsRepository->findBy([], ['id' => 'DESC'], $limit=1);
                    if ($rapport) {
                        $dateR = $rapport[0]->getCreatedAt();
                        $now = date("Y/m/d");
                        if ($now == $dateR->format("Y/m/d") ) {
                            $NbrMig = $rapport[0]->getNbrMig() + 1 ;
                            $MontantMig = $rapport[0]->getMontantMig() + $montant ;
                            $rapport[0]->setNbrMig($NbrMig);
                            $rapport[0]->setMontantMig($MontantMig);
                            $entityManager->persist($rapport[0]);
                            $entityManager->flush();
                        } else {
                            $rapport = new Rapports();
                            $rapport->setNbrMig(1);
                            $rapport->setMontantMig($montant);
                            $entityManager->persist($rapport);
                            $entityManager->flush();
                        }
                    } else {
                        $rapport = new Rapports();
                        $rapport->setNbrMig(1);
                        $rapport->setMontantMig($montant);
                        $entityManager->persist($rapport);
                        $entityManager->flush();
                    }

                    $this->addFlash(
                        'success',
                        'Migration lancé. En attente de validation !'
                     );
                    return $this->redirectToRoute('app_migrations_index', [], Response::HTTP_SEE_OTHER);
                }else {
                    $this->addFlash(
                       'warning',
                       'Fonds Insuffisant ! Veuillez contact votre administrateur '
                    );
                    return $this->redirectToRoute('app_migrations_new', [], Response::HTTP_SEE_OTHER);
                }
            }
        }

        return $this->renderForm('migrations/new.html.twig', [
            'migration' => $migration,
        ]);
    }

    /**
     * @Route("/{id}", name="app_migrations_show", methods={"GET"})
     */
    public function show(Migrations $migration): Response
    {
        return $this->render('migrations/show.html.twig', [
            'migration' => $migration,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_migrations_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Migrations $migration, MigrationsRepository $migrationsRepository): Response
    {
        $form = $this->createForm(MigrationsType::class, $migration);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $migrationsRepository->add($migration, true);

            return $this->redirectToRoute('app_migrations_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('migrations/edit.html.twig', [
            'migration' => $migration,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_migrations_delete", methods={"POST"})
     */
    public function delete(Request $request, Migrations $migration, MigrationsRepository $migrationsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$migration->getId(), $request->request->get('_token'))) {
            $migrationsRepository->remove($migration, true);
        }

        return $this->redirectToRoute('app_migrations_index', [], Response::HTTP_SEE_OTHER);
    }
}
