<?php

namespace App\Controller;

use App\Entity\Rapports;
use App\Entity\Recrutements;
use App\Entity\Reabonnements;
use App\Form\RecrutementsType;
use App\Repository\StocksRepository;
use App\Repository\RapportsRepository;
use App\Repository\DecodeursRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\RecrutementsRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/dashboard/recrutements")
 */
class RecrutementsController extends AbstractController
{
    /**
     * @Route("/", name="app_recrutements_index", methods={"GET"})
     */
    public function index(RecrutementsRepository $recrutementsRepository): Response
    {
        return $this->render('recrutements/index.html.twig', [
            'recrutements' => $recrutementsRepository->findBy(['user' => $this->getUser()], ['id' => 'DESC']),
        ]);
    }

    /**
     * @Route("/new", name="app_recrutements_new", methods={"GET", "POST"})
     */
    public function new(Request $request, RapportsRepository $rapportsRepository, RecrutementsRepository $recrutementsRepository, EntityManagerInterface $entityManager, StocksRepository $stocksRepository, DecodeursRepository $decodeursRepository): Response
    {
        $recrutement = new Recrutements();
        $user = $this->getUser();
        $stocks = $stocksRepository->findAll(['user' => $user->getSocietes()]);

        if ($request->isMethod('POST')) {
            $submittedToken = $request->request->get('Rectoken');
            if($this->isCsrfTokenValid('add-rec', $submittedToken)){

                if ($request->request->get('nopt')) {
                    $nopt = $request->request->get('nopt');
                } else {
                    $nopt = 0;
                }

                if($request->request->get('formule') == "Access"){
                    $montantF = 5000 ;
                } elseif ($request->request->get('formule') == "Evasion") {
                    $montantF = 9800 ;
                } elseif ($request->request->get('formule') == "Access +") {
                    $montantF = 15000 ;
                } elseif ($request->request->get('formule') == "Evasion +") {
                    $montantF = 20000 ;
                } elseif ($request->request->get('formule') == "Tout Canal +") {
                    $montantF = 40000 ;
                }

                $decodeur = $decodeursRepository->findOneBy(['numero' => $request->request->get('decodeur')]);
                $PD = 5000;
                $montant = $montantF + $PD ;

                $recrutement->setMontant($montant);
                if ($user->getSolde() >= $montant) {

                    $user->setSolde($user->getSolde() - ($montant - 1000));
                    $entityManager->persist($user);
                    $entityManager->flush();
                    
                    $recrutement->setNomClient($request->request->get('nom'));
                    $recrutement->setPrenomClient($request->request->get('prenom'));
                    $recrutement->setPhoneClient($request->request->get('phone'));
                    $recrutement->setAdresseClient($request->request->get('quartier'));
                    $recrutement->setReference($request->request->get('decodeur'));
                    $recrutement->setFormule($request->request->get('formule'));
                    $recrutement->setUser($user);
                    $recrutement->setStatut(false);
                    $entityManager->persist($recrutement);
                    $entityManager->flush();

                    $decodeur->setStatut(true);
                    $entityManager->persist($decodeur);
                    $entityManager->flush();

                    $rapport = $rapportsRepository->findBy([], ['id' => 'DESC'], $limit=1);
                    if ($rapport) {
                        $dateR = $rapport[0]->getCreatedAt();
                        $now = date("Y/m/d");
                        if ($now == $dateR->format("Y/m/d") ) {
                            $NbrRec = $rapport[0]->getNbrRec() + 1 ;
                            $MontantRec = $rapport[0]->getMontantRec() + $montant ;
                            $rapport[0]->setNbrRec($NbrRec);
                            $rapport[0]->setMontantRec($MontantRec);
                            $entityManager->persist($rapport[0]);
                            $entityManager->flush();
                        } else {
                            $rapport = new Rapports();
                            $rapport->setNbrRec(1);
                            $rapport->setMontantRec($montant);
                            $entityManager->persist($rapport);
                            $entityManager->flush();
                        }
                    } else {
                        $rapport = new Rapports();
                        $rapport->setNbrRec(1);
                        $rapport->setMontantRec($montant);
                        $entityManager->persist($rapport);
                        $entityManager->flush();
                    }

                    $this->addFlash(
                        'success',
                        'Recrutement lancé. En attente de validation !'
                     );
                    return $this->redirectToRoute('app_recrutements_index', [], Response::HTTP_SEE_OTHER);
                } else {
                    $this->addFlash(
                       'warning',
                       'Fonds Insuffisant ! Veuillez contact votre administrateur '
                    );
                    return $this->redirectToRoute('app_recrutements_new', [], Response::HTTP_SEE_OTHER);
                }

            }
        }

        return $this->renderForm('recrutements/new.html.twig', [
            'recrutement' => $recrutement,
            'stocks' => $stocks,
        ]);
    }

    /**
     * @Route("/{id}", name="app_recrutements_show", methods={"GET"})
     */
    public function show(Recrutements $recrutement): Response
    {
        return $this->render('recrutements/show.html.twig', [
            'recrutement' => $recrutement,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_recrutements_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Recrutements $recrutement, RecrutementsRepository $recrutementsRepository): Response
    {
        $form = $this->createForm(RecrutementsType::class, $recrutement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $recrutementsRepository->add($recrutement, true);

            return $this->redirectToRoute('app_recrutements_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('recrutements/edit.html.twig', [
            'recrutement' => $recrutement,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_recrutements_delete", methods={"POST"})
     */
    public function delete(Request $request, Recrutements $recrutement, RecrutementsRepository $recrutementsRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$recrutement->getId(), $request->request->get('_token'))) {
            $recrutementsRepository->remove($recrutement, true);
        }

        return $this->redirectToRoute('app_recrutements_index', [], Response::HTTP_SEE_OTHER);
    }
}
