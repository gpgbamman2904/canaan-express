<?php

namespace App\Form;

use App\Entity\Reabonnements;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReabonnementsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Reference')
            ->add('NumAbonne')
            ->add('Phone')
            ->add('NbrMois')
            ->add('Formule')
            ->add('upgrade')
            ->add('OptionF')
            ->add('NbrMoisOF')
            ->add('Statut')
            ->add('Observation')
            ->add('User')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reabonnements::class,
        ]);
    }
}
