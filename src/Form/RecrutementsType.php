<?php

namespace App\Form;

use App\Entity\Recrutements;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecrutementsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('NomClient')
            ->add('PrenomClient')
            ->add('AdresseClient')
            ->add('PhoneClient')
            ->add('OptionF')
            ->add('NbrMoisOF')
            ->add('Reference')
            ->add('Formule')
            ->add('NbrMois')
            ->add('Statut')
            ->add('FinRea')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recrutements::class,
        ]);
    }
}
